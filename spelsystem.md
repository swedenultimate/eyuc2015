Brett led
=========

No. 1
-----

    p   .  .  . .  .
                     .
                     .
      .              .
     .    ^        ^  
    .  x1 x2      x3   x4
    .   v             
    .         .         
    p             . 
                      .
                         _
     x                   o
                x

* 1:an rycket på fel sidan för att rensa
* 2:an fintar mot djupet och möter
* 3:an går på djupet och får en lång


Växel
-----

      p  .   .   .   .   .   .
                             .
                             .
                        .    .
                      .   .  .    
                    .     ^  . 
       p          .   x1  x2 .
        .        p    v     .
          .            . .
             .          _
                . . . . o
             
* 1:an möter för att sedan gå på djupet
* 2:an finat mot djupet för att sedan möte
* dischållaren går på en give'n'go diagonalt
* 2:an får en lång


Smalt led
=========

dump o sving
------------

             | x
             | x  
             | x2 > . . . . . p
             | x1 v
                  .
                  . 
        o\        .
                  p

* 1:an kommer på dump
* 2:an kommer på sving
* Dischållaren ställer sig längst bak i ledet. 
* 1:an ställer sig längst bak i ledet
* Kör dump sving åt andra hållet

Viktigt att rotera ordentligt så man kan slå dump-passen 



