European Youth Ultimate Championships 2015
==========================================

Dokumentbibliotek för U17 och U20 EM i Frankfurt.


Spelsystem och övningar
----------------------

* [Spelsystem](spelsystem.md)
* [Hoppspänstprogram](utskick/hoppspanst.md)


Övningar man kan göra hemma
--------------------------

* [utskick efter läger](utskick)


Läger
----

Lite om organisation och läger.

Följande upplägg användes för u17 open:

 * Ett öppet läger med uttagningar. C:a 25 personer kom till detta. Alla fick besked
   under söndagen om de var uttagna eller ej.
 * Ytterligare ett läger med truppen som tagits ut.

Schema för lördagen (andra lägret):

 * 7:30 frukost
 * 9:00 Bilar till planerna
 * 11:30 lunch
 * c:a 14 mellanmål
 * 16:30-17 bilar tillbaka till login
 * middag
 * rules accredation för de som inte gjort detta själva

Schema för söndagen:

 * 7:30 frukost
 * 10:30 mellanmål
 * 12:00 dusch och sedan lunch
 * 13-14 avslutande information och hemfärd


Kommunikation
------------

Följande används för att kommunicera:

 * Maillista med alla deltagarna samt föräldrar för de som inte är 18 (Bill har denna)
 * [U17 open facebook-sida](https://www.facebook.com/groups/754935854627283/?fref=ts)
 * [U17 open instagram](https://instagram.com/u17opensweden/)
 * [U17 girls instagram](https://instagram.com/u17girlssweden/)


Länkar
-----

 * [EYUC 2015](http://eyuc2015.ultimatecentral.com)
 * [WFDF Rules](http://rules.wfdf.org) - alla spelare måste göra accredation testet
 * [Ultimate Central](http://ultimatecentral.com) - alla spelare måste skapa ett konto här
 * [Rules accredation progile](http://rules.wfdf.org/component/users/?view=login) - här finns 
   ditt ID om du gjort provet.

Om dokumentarkivet
-----------------

Github fungerar ungefär som Dropbox men har bättre stöd för att dela filer mellan flera personer. Text-filer (som den här) skapas enkelt på webben. Filer kan också laddas upp och ner med hjälp ev ett Github program.

* [Hur man redigerar filer](https://help.github.com/articles/editing-files-in-your-repository/)
* [Hur man redigerar filer (del två)](https://help.github.com/articles/github-flavored-markdown/)

* Github program för [Windows](https://windows.github.com)
* Github progam för [Mac](https://mac.github.com)


Övrigt
-----
 
* [Ultimateregler på Svenska](http://ultimateregler.github.io)

